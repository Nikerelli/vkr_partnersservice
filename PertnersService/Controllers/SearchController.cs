﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using CoreService.Contract;
using CoreService.Contract.Enums;
using CoreService.Contract.Request;
using CoreService.Contract.Response;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using JsonConverter = System.Text.Json.Serialization.JsonConverter;

namespace PertnersService.Controllers
{
    [Route("search")]
    public class SearchController : Controller
    {
        private static Dictionary<string, IList<string>> Iatas = new Dictionary<string, IList<string>>();

        /// <summary>
        ///     Генерация значений в конструкторе
        /// </summary>
        static SearchController()
        {
            Iatas.Add("LED", new List<string>{ "LED" });
            Iatas.Add("KZN", new List<string> { "KZN" });
            Iatas.Add("MOW", new List<string> { "SVO", "VKO", "DME" });
        }

        [HttpPost("pegas")]
        public ActionResult PostPegas([FromBody] RequestFilter filter)
        {
            var rd = new Random();
            var trips = new List<Trip>();
            for (int i = 0; i < 15; i++)
            {
                var cities = GenerateTripCities();

                trips.Add(new Trip
                {
                    Route = new List<RoutePoint>
                    {
                        GenerateRoutePoint(cities.Item1,cities.Item2)
                    },
                    CompanyName = "Pegas",
                    DepartureCityIata = cities.Item1,
                    ArrivalCityIata = cities.Item2,
                    TotalFlightpDuration = TimeSpan.FromMinutes(rd.Next(0, 120)).ToString(),
                    TotalTripDuration = TimeSpan.FromMinutes(rd.Next(0, 240)).ToString(),
                    FlightClass = (FlightClass)rd.Next(0,3),
                    MeelIncluded = rd.Next(1,3) % 2 == 0,
                    FromCache = false
                });
            }

            if (filter != null)
            {
                var tripForFilter = new Trip
                {
                    Route = new List<RoutePoint>
                    {
                        GenerateRoutePoint(filter.Route.FirstOrDefault().DepartureIata,
                            filter.Route.FirstOrDefault().ArrivalIata)
                    },
                    CompanyName = "Pobeda",
                    DepartureCityIata = filter.Route.FirstOrDefault().DepartureIata,
                    ArrivalCityIata = filter.Route.FirstOrDefault().ArrivalIata,
                    TotalFlightpDuration = TimeSpan.FromMinutes(rd.Next(0, 120)).ToString(),
                    TotalTripDuration = TimeSpan.FromMinutes(rd.Next(0, 240)).ToString(),
                    FlightClass = (FlightClass)rd.Next(0, 3),
                    MeelIncluded = rd.Next(1, 3) % 2 == 0,
                    FromCache = false
                };
                tripForFilter.Route.FirstOrDefault().DepartureDate = filter.Route.FirstOrDefault().DepartureDate;
                tripForFilter.FlightClass =
                    filter.FlightClass.HasValue ? filter.FlightClass.Value : FlightClass.Economy;
            }

            return Json(FilterTrips(trips, filter));
        }

        [HttpPost("pobeda")]
        public ActionResult PostPobeda([FromBody] RequestFilter filter)
        {
            var rd = new Random();
            var trips = new List<Trip>();
            for (int i = 0; i < 15; i++)
            {
                var cities = GenerateTripCities();

                trips.Add(new Trip
                {
                    Route = new List<RoutePoint>
                    {
                        GenerateRoutePoint(cities.Item1,cities.Item2)
                    },
                    CompanyName = "Pobeda",
                    DepartureCityIata = cities.Item1,
                    ArrivalCityIata = cities.Item2,
                    TotalFlightpDuration = TimeSpan.FromMinutes(rd.Next(0, 120)).ToString(),
                    TotalTripDuration = TimeSpan.FromMinutes(rd.Next(0, 240)).ToString(),
                    FlightClass = (FlightClass)rd.Next(0, 3),
                    MeelIncluded = rd.Next(1, 3) % 2 == 0,
                    FromCache = false
                });

            }

            if (filter != null)
            {
                var tripForFilter = new Trip
                {
                    Route = new List<RoutePoint>
                    {
                        GenerateRoutePoint(filter.Route.FirstOrDefault().DepartureIata,
                            filter.Route.FirstOrDefault().ArrivalIata)
                    },
                    CompanyName = "Pobeda",
                    DepartureCityIata = filter.Route.FirstOrDefault().DepartureIata,
                    ArrivalCityIata = filter.Route.FirstOrDefault().ArrivalIata,
                    TotalFlightpDuration = TimeSpan.FromMinutes(rd.Next(0, 120)).ToString(),
                    TotalTripDuration = TimeSpan.FromMinutes(rd.Next(0, 240)).ToString(),
                    FlightClass = (FlightClass)rd.Next(0, 3),
                    MeelIncluded = rd.Next(1, 3) % 2 == 0,
                    FromCache = false
                };
                tripForFilter.Route.FirstOrDefault().DepartureDate = filter.Route.FirstOrDefault().DepartureDate;
                tripForFilter.FlightClass =
                    filter.FlightClass.HasValue ? filter.FlightClass.Value : FlightClass.Economy;
            }

            return Json(FilterTrips(trips, filter));
        }

        private (string, string) GenerateTripCities()
        {
            var rd = new Random();
            var departuerCity = rd.Next(0, 3);
            var arrivalCity = rd.Next(0, 3);

            while (departuerCity.Equals(arrivalCity))
            {
                arrivalCity = rd.Next(0, 3);
            }

            var cities = Iatas.Keys.Select(x => x).ToArray();

            return (cities[departuerCity], cities[arrivalCity]);
        }

        private RoutePoint GenerateRoutePoint(string departureCity, string arrivalCity)
        {
            var rd = new Random();

            return new RoutePoint
            {
                DepartureDate = DateTime.Now.Date.Date.AddDays(rd.Next(0,5)),
                DepartureIata = Iatas[departureCity][rd.Next(0, Iatas[departureCity].Count)],
                ArrivalIata = Iatas[arrivalCity][rd.Next(0, Iatas[arrivalCity].Count)]
            };
        }

        private List<Trip> FilterTrips(List<Trip> trips, RequestFilter filter)
        {
            if (filter == null)
            {
                return trips;
            }

            var filterRoute = filter.Route.FirstOrDefault();

            var filteredTrips = trips.Where(t => t.Route.FirstOrDefault().DepartureIata.Equals(filterRoute.DepartureIata) &&
                                     t.Route.FirstOrDefault().ArrivalIata.Equals(filterRoute.ArrivalIata)).ToList();

            if (filter.FlightClass.HasValue)
            {
                filteredTrips = trips.Where(x => x.FlightClass.Equals(filter.FlightClass)).ToList();
            }

            if (filter.Route.FirstOrDefault().DepartureDate.HasValue)
            {
                filteredTrips = trips.Where(x => x.Route.FirstOrDefault().DepartureDate.Equals(filter.Route.FirstOrDefault().DepartureDate)).ToList();
            }

            return trips;
        }
    }
}