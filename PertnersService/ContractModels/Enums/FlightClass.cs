﻿namespace CoreService.Contract.Enums
{
    public enum FlightClass
    {
        Economy = 0,
        Business,
        FirstClass,
    }
}
