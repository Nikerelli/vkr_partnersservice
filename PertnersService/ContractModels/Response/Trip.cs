﻿using System;
using System.Collections.Generic;
using CoreService.Contract.Enums;
using CoreService.Contract.Request;

namespace CoreService.Contract.Response
{
    public class Trip
    {
        public string CompanyName { get; set; }
        public IList<RoutePoint> Route { get; set; }

        public FlightClass? FlightClass { get; set; }

        public bool MeelIncluded { get; set; }

        public string TotalFlightpDuration { get; set; }
        public string TotalTripDuration { get; set; }

        public string DepartureCityIata { get; set; }
        public string ArrivalCityIata { get; set; }

        public bool FromCache { get; set; }
    }
}
